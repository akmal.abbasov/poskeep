/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#include <gtest/gtest.h>
#include "./instrument.h"

namespace {

TEST(Instrument, Getters) {
    poskeep::exchange::Instrument instrument("ABB", "SEK", "Asea Brown Boveri");
    EXPECT_EQ("ABB", instrument.name());
    EXPECT_EQ("SEK", instrument.currency());
    EXPECT_EQ("Asea Brown Boveri", instrument.issuer());
}

TEST(Instrument, ToStr) {
    poskeep::exchange::Instrument instrument("ABB", "SEK", "Asea Brown Boveri");
    EXPECT_EQ("ABB        | SEK   | Asea Brown Boveri   ", instrument.ToStr());
}

}  // namespace
