/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#include <gtest/gtest.h>

#include "./instrument.h"
#include "./exchange.h"
#include "./utils.h"

namespace {

TEST(Exchange, AddInstrument) {
    poskeep::exchange::Exchange exchange;
    EXPECT_EQ(false, exchange.IsInstrumentExist("ABB"));

    poskeep::exchange::Instrument instrument("ABB", "SEK", "Asea Brown Beveri");
    exchange.AddInstrument(instrument);

    EXPECT_EQ(true, exchange.IsInstrumentExist("ABB"));
}


TEST(Exchange, AddDuplicateInstrument) {
    poskeep::exchange::Exchange exchange;
    poskeep::exchange::Instrument instrument("ABB", "SEK", "Asea Brown Beveri");

    exchange.AddInstrument(instrument);
    EXPECT_THROW(
        exchange.AddInstrument(instrument),
        std::invalid_argument
    );
}


TEST(Exchange, MakeInvalidTrade) {
    poskeep::exchange::Exchange exchange;
    EXPECT_THROW(
        exchange.MakeTrade(
            poskeep::exchange::Trade(
                "ABB",
                10,
                12.5,
                "Stock",
                poskeep::utils::CurrTime(),
                "Equity Desk",
                "Swedbank",
                "OMX"
            )
        ),
        std::invalid_argument
    );
}

}  // namespace
