/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#include <ctime>
#include <gtest/gtest.h>

#include "trade.h"

namespace {

TEST(Trade, Getters) {
    // Get current time
    std::time_t currTime = std::chrono::system_clock::to_time_t(
                               std::chrono::system_clock::now()
                           );

    poskeep::exchange::Trade trade("ABB", 1, 3, "Stock", currTime, "Equity Desk", "Swedbank", "OMX");
    EXPECT_EQ("ABB", trade.instrument());
    EXPECT_EQ(1, trade.quantity());
    EXPECT_EQ(3.0, trade.price());
    EXPECT_EQ("Stock", trade.portofolio());
    EXPECT_EQ(std::ctime(&currTime), trade.time());
    EXPECT_EQ("Equity Desk", trade.acquirer());
    EXPECT_EQ("Swedbank", trade.counterparty());
    EXPECT_EQ("OMX", trade.marketplace());
}

}  // namespace
