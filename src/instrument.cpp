/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#include <iomanip>
#include <sstream>
#include <string>

#include "./instrument.h"

namespace poskeep {

namespace exchange {

Instrument::Instrument() {
}

Instrument::Instrument(const std::string& name,
                       const std::string& currency,
                       const std::string& issuer)
    : name_(name),
      currency_(currency),
      issuer_(issuer) {
}


std::string Instrument::name() const {
    return name_;
}


std::string Instrument::currency() const {
    return currency_;
}


std::string Instrument::issuer() const {
    return issuer_;
}


std::string Instrument::ToStr() const {
    std::stringstream str_stream;
    str_stream << std::setw(10) << std::left << name_     << " | "
               << std::setw(5)  << std::left << currency_ << " | "
               << std::setw(20) << std::left << issuer_;

    return str_stream.str();
}


std::ostream & operator<<(std::ostream     & stream,
                          const Instrument & instrument) {
    stream << instrument.ToStr();
    return stream;
}

}  // namespace exchange

}  // namespace poskeep
