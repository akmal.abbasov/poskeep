/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#ifndef POSKEEP_TRADE_H_
#define POSKEEP_TRADE_H_

#include <string>
#include <ctime>

namespace poskeep {

namespace exchange {

class Trade {
 public:
    Trade();

    Trade(const std::string & instrument_name,
          const int quantity,
          const double price,
          const std::string & portofolio,
          const std::time_t & time,
          const std::string & acquirer,
          const std::string & counterparty,
          const std::string & marketplace);

    std::string instrument() const;
    int quantity() const;
    double price() const;
    std::string portofolio() const;
    std::string time() const;
    std::string acquirer() const;
    std::string counterparty() const;
    std::string marketplace() const;

    std::string ToStr() const;

    friend std::ostream & operator<<(std::ostream & stream,
                                     const Trade  & trade);

 private:
    std::string instrument_;
    int quantity_;
    double price_;
    std::string portofolio_;
    std::time_t time_;
    std::string acquirer_;
    std::string counterparty_;
    std::string marketplace_;
};

}  // namespace exchange

}  // namespace poskeep

#endif  // POSKEEP_TRADE_H_
