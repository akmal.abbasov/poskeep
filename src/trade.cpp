/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#include <iomanip>
#include <sstream>
#include <string>

#include "./trade.h"

namespace poskeep {

namespace exchange {

    Trade::Trade() {
    }


    Trade::Trade(const std::string & instrument_name,
                             const int quantity,
                             const double price,
                             const std::string & portofolio,
                             const std::time_t & time,
                             const std::string & acquirer,
                             const std::string & counterparty,
                             const std::string & marketplace)
        : instrument_(instrument_name),
          quantity_(quantity),
          price_(price),
          portofolio_(portofolio),
          time_(time),
          acquirer_(acquirer),
          counterparty_(counterparty),
          marketplace_(marketplace) {
    }


    std::string Trade::instrument() const {
        return instrument_;
    }


    int Trade::quantity() const {
        return quantity_;
    }


    double Trade::price() const {
        return price_;
    }


    std::string Trade::portofolio() const {
        return portofolio_;
    }


    std::string Trade::time() const {
        return std::ctime(&time_);
    }


    std::string Trade::acquirer() const {
        return acquirer_;
    }


    std::string Trade::counterparty() const {
        return counterparty_;
    }


    std::string Trade::marketplace() const {
        return marketplace_;
    }


    std::string Trade::ToStr() const {
        std::stringstream str_stream;
        str_stream << std::setw(10) << std::left << instrument_   << " | "
                   << std::setw(5)  << std::left << quantity_     << " | "
                   << std::setw(5)  << std::left << price_        << " | "
                   << std::setw(8)  << std::left << portofolio_   << " | "
                   << std::setw(12) << std::left << acquirer_     << " | "
                   << std::setw(15) << std::left << counterparty_ << " | "
                   << std::setw(5)  << std::left << marketplace_  << " | "
                   << std::setw(12) << std::left << std::ctime(&time_);

        return str_stream.str();
    }


    std::ostream & operator<<(std::ostream & stream,
                              const Trade  & trade) {
        stream << trade.ToStr();
        return stream;
    }

}  // namespace exchange

}  // namespace poskeep
