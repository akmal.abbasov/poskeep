/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>

#include "./exchange.h"
#include "./position.h"
#include "./utils.h"

namespace poskeep {

namespace exchange {

void Exchange::AddInstrument(const Instrument & instrument) {
    if (!IsInstrumentExist(instrument.name())) {
        // Instruments are kept in hashmap, and instrument
        // name in uppercase is used as key
        instruments_[poskeep::utils::StringToUpper(instrument.name())] = instrument;

    } else {
        throw std::invalid_argument("Instrument already exist in exchange.");
    }
}


void Exchange::ListInstruments() const {
    std::for_each(instruments_.begin(), instruments_.end(), [](const auto & p) {
        std::cout << p.second << std::endl;
    });
}


bool Exchange::IsInstrumentExist(const std::string & instrument_name) const {
    // Instruments are kept in hashmap, and instrument
    // name in uppercase is used as key
    return instruments_.find(poskeep::utils::StringToUpper(instrument_name)) != instruments_.end();
}


void Exchange::MakeTrade(const Trade& trade) {
    if (IsInstrumentExist(trade.instrument())) {
        trades_.push_back(trade);

    } else {
        throw std::invalid_argument("Given instrument does not exist.");
    }
}


void Exchange::ListTrades() const {
    std::for_each(trades_.begin(), trades_.end(), [](const auto & trade) {
       std::cout << trade << std::endl;
    });
}


// Lists Total Traded Amount(TTA) for each Instument.
// Trades information is used to do required calculations.
void Exchange::ListPositions() const {
    // Position for each Instrument
    std::unordered_map<std::string, Position> positions;

    for (const auto & trade : trades_) {
        // Create aliases
        auto instrument_name = poskeep::utils::StringToUpper(trade.instrument());

        auto position = positions.find(instrument_name);
        if (position == positions.end()) {
            Instrument instrument = instruments_.at(instrument_name);
            positions[instrument_name] = { instrument, trade.price() * trade.quantity() };

        } else {
            positions[instrument_name].total_trade_amount_ += (trade.price() * trade.quantity());
        }
    }

    std::for_each(positions.begin(), positions.end(), [](const auto & position) {
       std::cout << position.second << std::endl;
    });
}


// Lists Total Traded Amount(TTA) for each Instument in Portofolio.
// Trades information is used to do required calculations.
void Exchange::ListPositionsPerPortofolio() const {
    std::unordered_map<std::string, std::unordered_map<std::string, Position> > positions_per_portofolio;

    for (const auto & trade : trades_) {
        // Create aliases
        auto portofolio_name = poskeep::utils::StringToUpper(trade.portofolio());
        auto instrument_name = poskeep::utils::StringToUpper(trade.instrument());

        auto portofolio = positions_per_portofolio.find(portofolio_name);
        if (portofolio == positions_per_portofolio.end()) {
            positions_per_portofolio.emplace(portofolio_name, std::unordered_map<std::string, Position>());
        }

        auto instrument = positions_per_portofolio[portofolio_name].find(instrument_name);
        if (instrument == positions_per_portofolio[portofolio_name].end()) {
            positions_per_portofolio[portofolio_name][instrument_name] = {
                instruments_.at(instrument_name),
                trade.price() * trade.quantity()
            };

        } else {
            positions_per_portofolio[portofolio_name][instrument_name].total_trade_amount_
                    += (trade.price() * trade.quantity());
        }
    }

    for (const auto & portofolio : positions_per_portofolio) {
        std::cout << portofolio.first << std::endl;
        for (const auto & instrument : portofolio.second) {
            std::cout << instrument.second << std::endl;
        }
        std::cout << std::endl;
    }
}

}  // namespace exchange

}  // namespace poskeep
