/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#ifndef POSKEEP_UTILS_H_
#define POSKEEP_UTILS_H_

#include <string>

#include "./trade.h"
#include "./exchange.h"
#include "./instrument.h"

namespace poskeep {

namespace utils {

    // Clears console screen
    void ClearScreen();

    // Returns current time
    time_t CurrTime();

    // Prints dialog to add instrument. Returns instrument created from
    // user input
    poskeep::exchange::Instrument AddInstrument();

    // Prints dialog to make trade. Returns trade created from user input
    poskeep::exchange::Trade MakeTrade();

    // Adds dummy data to exchange
    void LoadData(poskeep::exchange::Exchange * exchange);

    // Returns copy of string in uppercase
    std::string StringToUpper(const std::string & str);

}  // namespace utils

}  // namespace poskeep

#endif  // POSKEEP_UTILS_H_
