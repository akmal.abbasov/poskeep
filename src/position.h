/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#ifndef POSKEEP_POSITION_H_
#define POSKEEP_POSITION_H_

#include <string>
#include "./instrument.h"

namespace poskeep {

namespace exchange {

struct Position {
    Instrument instrument_;
    double     total_trade_amount_;

    std::string ToStr() const;

    friend std::ostream & operator<<(std::ostream   & stream,
                                     const Position & position);
};

}  // namespace exchange

}  // namespace poskeep

#endif  // POSKEEP_POSITION_H_
