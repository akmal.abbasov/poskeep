/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#ifndef POSKEEP_EXCHANGE_H_
#define POSKEEP_EXCHANGE_H_

#include <string>
#include <unordered_map>
#include <vector>

#include "./instrument.h"
#include "./trade.h"

namespace poskeep {

namespace exchange {

class Exchange {
 public:
    // Adds new instrument to exchange
    void AddInstrument(const Instrument & instrument);

    // Returns true if instrument exist in exchange
    bool IsInstrumentExist(const std::string & instrument_name) const;

    // Prints instruments in exchange to standard out
    void ListInstruments() const;

    // Prints positions for each instruments to standard out
    void ListPositions() const;

    // Prints positions for instruments in portofolio to stardard out
    void ListPositionsPerPortofolio() const;

    // Makes a trade in exchange
    void MakeTrade(const Trade & trade);

    // Print trades history to stardard output
    void ListTrades() const;

 private:
    std::unordered_map<std::string, Instrument> instruments_;
    std::vector<Trade> trades_;
};

}  // namespace exchange

}  // namespace poskeep

#endif  // POSKEEP_EXCHANGE_H_
