/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#include <chrono>
#include <cstdlib>
#include <iostream>
#include <string>

#include "./exchange.h"
#include "./instrument.h"
#include "./trade.h"
#include "./utils.h"


enum MenuCommands {
    ADD_INSTRUMENT = 1,
    LIST_INSTRUMENTS,
    MAKE_TRADE,
    LIST_TRADES,
    LIST_POSITIONS,
    LIST_POSITIONS_PER_PORTOFOLIO,
    EXIT
};


void PrintErrorMsg(const std::string & what) {
    std::cout << "\nFailure: " << what << std::endl;
}


void printMenu() {
    std::cout << "Menu Options:\n"
              << "1 - Add Instrument\n"
              << "2 - List Instruments\n"
              << "3 - Make Trade\n"
              << "4 - List Trades\n"
              << "5 - List Positions\n"
              << "6 - List Positions per Portofolio\n"
              << "7 - Exit\n";
}


void AddInstrumentToExchange(poskeep::exchange::Exchange * exchange) {
    try {
        exchange->AddInstrument(poskeep::utils::AddInstrument());
    } catch(const std::invalid_argument & e) {
        PrintErrorMsg(e.what());
    }
}


void MakeTradeInExchange(poskeep::exchange::Exchange * exchange) {
    poskeep::exchange::Trade trade = poskeep::utils::MakeTrade();
    try {
        exchange->MakeTrade(trade);
    } catch(const std::invalid_argument & e) {
        PrintErrorMsg(e.what());
    }
}


int main(int argc, char** argv) {
    poskeep::exchange::Exchange exchange;

    // Fill exchange with some data
    poskeep::utils::LoadData(&exchange);

    int choice;
    while (true) {
        poskeep::utils::ClearScreen();
        printMenu();

        // Read user input
        std::cin >> choice;

        if (choice == MenuCommands::EXIT)
            break;

        poskeep::utils::ClearScreen();

        switch (choice) {
            case MenuCommands::ADD_INSTRUMENT:
                AddInstrumentToExchange(&exchange);
                break;

            case MenuCommands::LIST_INSTRUMENTS:
                exchange.ListInstruments();
                std::cin.ignore();  // Ignore \n added after user input
                break;

            case MenuCommands::MAKE_TRADE:
                MakeTradeInExchange(&exchange);
                break;

            case MenuCommands::LIST_TRADES:
                exchange.ListTrades();
                std::cin.ignore();  // Ignore \n added after user input
                break;

            case MenuCommands::LIST_POSITIONS:
                exchange.ListPositions();
                std::cin.ignore();  // Ignore \n added after user input
                break;

            case MenuCommands::LIST_POSITIONS_PER_PORTOFOLIO:
                exchange.ListPositionsPerPortofolio();
                std::cin.ignore();  // Ignore \n added after user input
                break;

            default:
                PrintErrorMsg("Invalid input, exiting...");
                return 1;
        }

        std::cout << "\nPress Enter to continue...\n";
        std::cin.ignore();
    }

    return 0;
}
