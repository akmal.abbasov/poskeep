/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#ifndef POSKEEP_INSTRUMENT_H_
#define POSKEEP_INSTRUMENT_H_

#include <string>

namespace poskeep {

namespace exchange {

class Instrument {
 public:
    Instrument();
    Instrument(const std::string & name,
               const std::string & currency,
               const std::string & issuer);

    std::string name() const;
    std::string currency() const;
    std::string issuer() const;

    std::string ToStr() const;

    friend std::ostream & operator<<(std::ostream     & stream,
                                     const Instrument & instrument);

 private:
    std::string name_;
    std::string currency_;
    std::string issuer_;
};

}  // namespace exchange

}  // namespace poskeep

#endif  // POSKEEP_INSTRUMENT_H_
