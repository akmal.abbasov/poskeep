/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <string>

#include "./utils.h"
#include "./exchange.h"
#include "./instrument.h"
#include "./trade.h"

namespace poskeep {

namespace utils {

void ClearScreen() {
    // CSI[2J clears screen, CSI[H moves the cursor to top-left corner
    std::cout << "\x1B[2J\x1B[H";
}


time_t CurrTime() {
    return std::chrono::system_clock::to_time_t(
                std::chrono::system_clock::now());
}


void ReadMultiWordString(std::istream * stream, std::string * multi_word_string) {
    stream->ignore();  // Ignore \n char left in the input stream
    std::getline(*stream, *multi_word_string);
}


poskeep::exchange::Instrument AddInstrument() {
    std::string name;
    std::string currency;
    std::string issuer;

    std::cout << "Add Instrument Dialog\n";
    std::cout << "Name: ";
    ReadMultiWordString(&std::cin, &name);

    std::cout << "Currency: ";
    std::cin >> currency;

    std::cout << "Issuer: ";
    ReadMultiWordString(&std::cin, &issuer);

    return poskeep::exchange::Instrument(name, currency, issuer);
}


poskeep::exchange::Trade MakeTrade() {
    std::string instrument_name;
    int quantity;
    double price;
    std::string portofolio;
    std::string acquirer;
    std::string counterparty;
    std::string marketplace;

    std::cout << "Make Trade Dialog\n";

    std::cout << "Instrument Name: ";
    ReadMultiWordString(&std::cin, &instrument_name);

    std::cout << "Quantity: ";
    std::cin >> quantity;

    std::cout << "Price: ";
    std::cin >> price;

    std::cout << "Portofolio: ";
    ReadMultiWordString(&std::cin, &portofolio);

    std::cout << "Acquirer: ";
    ReadMultiWordString(&std::cin, &acquirer);

    std::cout << "Counterparty: ";
    ReadMultiWordString(&std::cin, &counterparty);

    std::cout << "Marketplace: ";
    ReadMultiWordString(&std::cin, &marketplace);

    // Get current time
    std::time_t currTime = std::chrono::system_clock::to_time_t(
                               std::chrono::system_clock::now());

    return poskeep::exchange::Trade(instrument_name,
                                    quantity,
                                    price,
                                    portofolio,
                                    currTime,
                                    acquirer,
                                    counterparty,
                                    marketplace);
}


void CreateInstuments(poskeep::exchange::Exchange * exchange) {
    exchange->AddInstrument(poskeep::exchange::Instrument("ABB", "SEK", "Asea Brown Boveri"));
    exchange->AddInstrument(poskeep::exchange::Instrument("Ericsson B", "SEK", "LM Ericsson"));
    exchange->AddInstrument(poskeep::exchange::Instrument("VAB", "USD", "VOLVO AB"));
}


void CreateTrades(poskeep::exchange::Exchange * exchange) {
    exchange->MakeTrade(
        poskeep::exchange::Trade(
            "ABB",
            10,
            12.5,
            "Stock",
            poskeep::utils::CurrTime(),
            "Equity Desk",
            "Swedbank",
            "OMX"));

    exchange->MakeTrade(
        poskeep::exchange::Trade(
            "Ericsson B",
            12,
            9.7,
            "Stock",
            poskeep::utils::CurrTime(),
            "Equity Desk",
            "Deutsche Bank",
            "OMX"));

    exchange->MakeTrade(
        poskeep::exchange::Trade(
            "VAB",
            102,
            0.57,
            "Bond",
            poskeep::utils::CurrTime(),
            "Equity Desk",
            "SEB",
            "OMX"));
}


void LoadData(poskeep::exchange::Exchange * exchange) {
    CreateInstuments(exchange);
    CreateTrades(exchange);
}


std::string StringToUpper(const std::string & str) {
    std::string lowercase_str = str;
    std::transform(lowercase_str.begin(),
                   lowercase_str.end(),
                   lowercase_str.begin(),
                   ::toupper);

    return lowercase_str;
}


}  // namespace utils

}  // namespace poskeep
