/* Copyright 2017 Akmal Abbasov.  All rights reserved. */

#include <iomanip>
#include <sstream>
#include <string>

#include "./position.h"

namespace poskeep {

namespace exchange {

std::string Position::ToStr() const {
    std::stringstream str_stream;
    str_stream << std::setw(30) << instrument_.ToStr()  << " | "
               << std::setw(6)  << std::left << total_trade_amount_;

    return str_stream.str();
}


std::ostream & operator<<(std::ostream   & stream,
                          const Position & position) {
    stream << position.ToStr();
    return stream;
}

}  // namespace exchange

}  // namespace poskeep
