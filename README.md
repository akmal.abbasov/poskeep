# poskeep
Simple position keeping application

# Dependencies
* g++ with C++14 support;
* [googletest](https://github.com/google/googletest) to run unit tests;
* [cpplint](https://github.com/google/styleguide/tree/gh-pages/cpplint) C++ code linter.

## Building
`cd poskeep && make`

## Usage
Run `./poskeep` to start application.

## Design

### Requirements
* Create instruments of any kind;
* Make trades in market;
* Monitor position;
* Group trades on portofolio and instrument.

### Ambiguity
As I have limited knowledge about the context of the problem, I made the following assumptions:
* _Instuments_ have unique names;
* _Position_ is set of trades made with same instrument;
* Total Trade Amount calculated per instrument as follows: `tta = sum(trade.price * trade.quantity)`

### Data types
Trades are kept as `vector<Trade>`, while instruments are kept in `unordered_map` where the _key_ is 
the name of instrument in _uppercase_(to make it case insensitive). `unordered_map` is used as in average
it provides constant-time complexity for search, insertion, and removal of elements.  

### Memory allocation
As it's a small application, I decided to keep things simple, and didn't use dynamic 
memory allocation.
